from flask import *
from db import db
from models import User, Article, Comment, Category, Tag
import math
import hashlib
from datetime import datetime

home = Blueprint('home', __name__)


@home.context_processor
def context_processor():
    articles_count = Article.query.count()
    categories = Category.query.all()
    tags = Tag.query.all()
    recent_articles = Article.query.order_by(db.desc(Article.createdDate)).slice(0, 5).all()
    archives = db.session.query(db.func.date_format(Article.createdDate, '%Y-%m').label('date'),
                                db.func.count(Article.id)).group_by('date').order_by(db.desc('date')).all()
    return {
        'articles_count': articles_count,
        'categories': categories,
        'tags': tags,
        'recent_articles': recent_articles,
        'archives': archives
    }


@home.route('/')
def index():
    s = request.args.get('s')
    if s:
        articles = Article.query.filter(db.or_(Article.title.like(f'%{s}%'), Article.content.like(f'%{s}%'))).all()
        print(articles)
        return render_template('home/index.html', articles=articles)
    # 文章页数
    count = Article.query.count()
    pager = []
    for i in range(math.ceil(count / 10)):
        pager.append(str(i + 1))

    page = request.args.get('page')
    if not page:
        articles = Article.query.order_by(db.desc(Article.createdDate)).paginate(1, 10)
    else:
        articles = Article.query.order_by(db.desc(Article.createdDate)).paginate(int(page), 10)
    return render_template('home/index.html', articles=articles.items, pager=pager)


@home.route('/posts/<aid>')
def posts(aid):
    article = Article.query.get(aid)
    comments = Comment.query.filter_by(aid=aid).order_by(db.desc(Comment.createdDate)).all()
    return render_template('home/posts.html', article=article, comments=comments)


@home.route('/archives')
def archives_route():
    data = []
    archives = db.session.query(db.func.date_format(Article.createdDate, '%Y').label('date')).group_by('date').order_by(
        db.desc('date')).all()
    for archive in archives:
        data.append({
            archive[0]: Article.query.filter(db.extract('year', Article.createdDate) == archive[0]).order_by(
                db.desc(Article.createdDate)).all()
        })
    return render_template('home/common.html', data=data)


@home.route('/archives/<year>/<month>')
def archive_query(year, month):
    data = [{
        year: Article.query.filter(db.extract('year', Article.createdDate) == year,
                                   db.extract('month', Article.createdDate) == month
                                   ).order_by(
            db.desc(Article.createdDate)).all()
    }]
    return render_template('home/common.html', data=data)


@home.route('/tags')
def tags_route():
    tags = Tag.query.all()
    return render_template('home/tags.html', tags=tags)


@home.route('/tags/<name>')
def tags_query(name):
    data = []
    tag = Tag.query.filter_by(name=name).first()
    data.append({tag.name: tag.articles})
    return render_template('home/common.html', data=data)


@home.route('/categories')
def categories_route():
    articles = Article.query.all()
    return render_template('home/categories.html', articles=articles)


@home.route('/categories/<name>')
def categories_query(name):
    data = []
    category = Category.query.filter_by(name=name).first()
    data.append({category.name: category.articles})
    return render_template('home/common.html', data=data)


@home.route('/about')
def about():
    return render_template('home/about.html')


@home.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'GET':
        return render_template('home/login.html')
    else:
        email = request.form.get('email')
        password = request.form.get('password')
        user = User.query.filter_by(email=email).first()
        if not user:
            return render_template('home/login.html', msg_email='该邮箱不存在')
        else:
            if user.password != hashlib.md5(password.encode('utf8')).hexdigest():
                return render_template('home/login.html', msg_password='密码错误')
            session['user_id'] = user.id
            user.lastLogin = datetime.now()
            db.session.commit()
            next_url = request.args.get('next')
            if next_url:
                return redirect(next_url)
            return redirect(url_for('admin.index'))


@home.route('/register', methods=['GET', 'POST'])
def register():
    if request.method == 'GET':
        return render_template('home/register.html')
    else:
        username = request.form.get('username')
        email = request.form.get('email')
        password = request.form.get('password')
        user = User.query.filter_by(email=email).first()
        if user:
            return render_template('home/register.html', msg_email='该邮箱已存在')
        print(username, email, hashlib.md5(password.encode('utf8')).hexdigest())
        user = User(username=username, email=email, password=hashlib.md5(password.encode('utf8')).hexdigest())
        db.session.add(user)
        db.session.commit()
        return render_template('home/register.html', msg='注册成功')


@home.route('/logout')
def logout():
    session.clear()
    next_page = request.args.get('next')
    if next_page:
        return redirect(next_page)
    else:
        return redirect(url_for('home.login'))
