from flask import *
from werkzeug.utils import secure_filename
from pathlib import Path
from datetime import datetime
from db import db
from models import Article, Comment, Category, Tag, User

admin = Blueprint('admin', __name__, url_prefix='/admin')


@admin.before_request
def before_request():
    if request.path == '/admin/add-comment':
        return
    user_id = session.get('user_id')
    if not user_id:
        return redirect(url_for('home.login'))
    user = User.query.get(user_id)
    if user.role == 2:
        return redirect(url_for('home.index'))


@admin.route('/')
def index():
    articles_count = Article.query.count()
    comments_count = Comment.query.count()
    return render_template('admin/index.html', articles_count=articles_count, comments_count=comments_count)


@admin.route('/write-post', methods=['GET', 'POST'])
def write_post():
    if request.method == 'GET':
        categories = Category.query.all()
        return render_template('admin/write-post.html', categories=categories)

    else:
        title = request.form.get('title')
        content = request.form.get('content')
        categories = request.form.getlist('category')
        tags = request.form.get('tags')

        article = Article(title=title, content=content, uid=session.get('user_id'))

        if not categories:
            category = Category.query.filter_by(name='未分类').first()
            article.categories.append(category)
        else:
            for category_id in categories:
                category = Category.query.filter_by(id=category_id).first()
                article.categories.append(category)

        if tags:
            for tag in tags.split(','):
                is_exist = Tag.query.filter_by(name=tag).first()
                if not is_exist:
                    article.tags.append(Tag(name=tag))
                else:
                    article.tags.append(is_exist)
        db.session.add(article)
        db.session.commit()
        return redirect(url_for('admin.manage_post'))


@admin.route('/edit-post', methods=['GET', 'POST'])
def edit_post():
    if request.method == 'GET':
        categories = Category.query.all()
        post_id = request.args.get('id')
        article = Article.query.filter_by(id=post_id).first()
        categories_id = []
        for category in article.categories:
            categories_id.append(category.id)
        return render_template('admin/edit-post.html',
                               categories=categories,
                               article=article,
                               categories_id=categories_id
                               )
    else:
        title = request.form.get('title')
        content = request.form.get('content')
        categories = request.form.getlist('category')
        tags = request.form.get('tags')
        post_id = request.args.get('id')
        article = Article.query.get(post_id)

        article.title = title
        article.content = content

        if not categories:
            category = Category.query.filter_by(name='未分类').first()
            article.categories.append(category)
        else:
            for category_id in categories:
                category = Category.query.filter_by(id=category_id).first()
                article.categories.append(category)

        if tags:
            for tag in tags.split(','):
                is_exist = Tag.query.filter_by(name=tag).first()
                if not is_exist:
                    article.tags.append(Tag(name=tag))
                else:
                    article.tags.append(is_exist)

        db.session.add(article)
        db.session.commit()

        return redirect(url_for('admin.manage_post'))


@admin.route('/manage-posts')
def manage_post():
    articles = Article.query.order_by(db.desc(Article.createdDate)).all()
    return render_template('admin/manage-posts.html', articles=articles)


@admin.route('/delete-post', methods=['POST'])
def delete_post():
    if request.method == 'POST':
        post_ids = request.form.getlist('ids[]')
        for post_id in post_ids:
            article = Article.query.filter_by(id=post_id).first()
            db.session.delete(article)
        db.session.commit()
        return 'success'


@admin.route('/manage-comments')
def manage_comment():
    comments = Comment.query.order_by(db.desc(Comment.createdDate)).all()
    return render_template('admin/manage-comments.html', comments=comments)


@admin.route('/add-comment', methods=['POST'])
def add_comment():
    article_id = request.form.get('aid')
    content = request.form.get('content')
    user_id = session.get('user_id')
    print(article_id, user_id, content)
    comment = Comment(content=content, aid=article_id, uid=user_id)
    db.session.add(comment)
    db.session.commit()
    return redirect(url_for('home.posts', aid=article_id))


@admin.route('/delete-comment', methods=['POST'])
def delete_comment():
    if request.method == 'POST':
        comment_ids = request.form.getlist('ids[]')
        for comment_id in comment_ids:
            comment = Comment.query.filter_by(id=comment_id).first()
            db.session.delete(comment)
        db.session.commit()
        return 'success'


@admin.route('/manage-categories')
def manage_categories():
    categories = Category.query.order_by(db.asc(Category.id)).all()
    category_id = request.args.get('id')
    if not category_id:
        return render_template('admin/manage-categories.html', categories=categories)
    cur_category = Category.query.get(category_id)
    return render_template('admin/manage-categories.html', categories=categories, cur_category=cur_category)


@admin.route('/add-category', methods=['POST'])
def add_category():
    if request.method == 'POST':
        name = request.form.get('name')
        category = Category(name=name)
        db.session.add(category)
        db.session.commit()
        return redirect(url_for('admin.manage_categories'))


@admin.route('/edit-category', methods=['POST'])
def edit_category():
    if request.method == 'POST':
        category_id = request.form.get('id')
        name = request.form.get('name')
        category = Category.query.get(category_id)
        category.name = name
        db.session.commit()
        return redirect(url_for('admin.manage_categories'))


@admin.route('/delete-category', methods=['POST'])
def delete_category():
    if request.method == 'POST':
        category_ids = request.form.getlist('ids[]')
        for category_id in category_ids:
            category = Category.query.get(category_id)
            db.session.delete(category)
        db.session.commit()
        return 'success'


@admin.route('/manage-tags')
def manage_tags():
    tags = Tag.query.order_by(db.asc(Tag.id)).all()
    tag_id = request.args.get('id')
    if not tag_id:
        return render_template('admin/manage-tags.html', tags=tags)
    cur_tag = Tag.query.get(tag_id)
    return render_template('admin/manage-tags.html', tags=tags, cur_tag=cur_tag)


@admin.route('/add-tag', methods=['POST'])
def add_tag():
    if request.method == 'POST':
        name = request.form.get('name')
        tag = Tag(name=name)
        db.session.add(tag)
        db.session.commit()
        return redirect(url_for('admin.manage_tags'))


@admin.route('/edit-tag', methods=['POST'])
def edit_tag():
    if request.method == 'POST':
        tag_id = request.form.get('id')
        name = request.form.get('name')
        tag = Tag.query.get(tag_id)
        tag.name = name
        db.session.commit()
        return redirect(url_for('admin.manage_tags'))


@admin.route('/delete-tag', methods=['POST'])
def delete_tag():
    if request.method == 'POST':
        tag_ids = request.form.getlist('ids[]')
        for tag_id in tag_ids:
            tag = Tag.query.get(tag_id)
            db.session.delete(tag)
        db.session.commit()
        return 'success'


@admin.route('/manage-users')
def manage_users():
    users = User.query.all()
    return render_template('admin/manage-users.html', users=users)


@admin.route('/delete-user', methods=['POST'])
def delete_user():
    ids = request.form.getlist('ids[]')
    for user_id in ids:
        user = User.query.get(user_id)
        db.session.delete(user)
    db.session.commit()
    return 'success'


ALLOWED_EXTENSIONS = {'txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'}


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@admin.route('/upload', methods=['GET', 'POST'])
def upload():
    if request.method == 'GET':
        return ''
    else:
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            suffix = secure_filename(file.filename).split('.')[-1]
            filename = datetime.now().strftime('%Y%m%d%H%M%S') + '.' + suffix
            file_path = Path('/', current_app.config['UPLOAD_FOLDER'], filename)
            file.save(Path(current_app.config['UPLOAD_FOLDER'], filename))
            return jsonify({
                'errno': 0,
                'data': [{
                    'url': str(file_path),
                    'alt': filename,
                    'href': str(file_path)
                }]
            })
