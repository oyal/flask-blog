from db import db

ac = db.Table('ac',
              db.Column('aid', db.Integer, db.ForeignKey('articles.id')),
              db.Column('cid', db.Integer, db.ForeignKey('categories.id'))
              )

at = db.Table('at',
              db.Column('aid', db.Integer, db.ForeignKey('articles.id')),
              db.Column('tid', db.Integer, db.ForeignKey('tags.id'))
              )


class User(db.Model):
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    username = db.Column(db.String(30), nullable=False)
    email = db.Column(db.String(30), nullable=False, unique=True)
    avatar = db.Column(db.Text, default='https://oyal.oss-cn-beijing.aliyuncs.com/img/202203192350138.png')
    password = db.Column(db.String(50), nullable=False)
    role = db.Column(db.Integer, nullable=False, server_default=db.text('2'))
    createdDate = db.Column(db.DateTime, nullable=False, server_default=db.text('CURRENT_TIMESTAMP'))
    lastLogin = db.Column(db.DateTime, nullable=False, server_default=db.text('CURRENT_TIMESTAMP'))


class Article(db.Model):
    __tablename__ = 'articles'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    title = db.Column(db.String(50), nullable=False)
    content = db.Column(db.Text)
    uid = db.Column(db.Integer, db.ForeignKey('users.id'))
    author = db.relationship('User', backref=db.backref('articles'))
    categories = db.relationship('Category', secondary=ac, backref=db.backref('articles'))
    tags = db.relationship('Tag', secondary=at, backref=db.backref('articles'))
    comments = db.relationship('Comment', backref=db.backref('article'))
    createdDate = db.Column(db.DateTime, nullable=False, server_default=db.text('CURRENT_TIMESTAMP'))
    updatedDate = db.Column(db.DateTime, nullable=False,
                            server_default=db.text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))


class Category(db.Model):
    __tablename__ = 'categories'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(20), unique=True)


class Tag(db.Model):
    __tablename__ = 'tags'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(20), unique=True)


class Comment(db.Model):
    __tablename__ = 'comments'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    content = db.Column(db.Text, nullable=False)
    uid = db.Column(db.Integer, db.ForeignKey('users.id'))
    user = db.relationship('User')
    aid = db.Column(db.Integer, db.ForeignKey('articles.id'))
    createdDate = db.Column(db.DateTime, nullable=False, server_default=db.text('CURRENT_TIMESTAMP'))
