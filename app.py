from flask import *
import config
from db import db
from models import *
from flask_migrate import Migrate
from models import User
import re

from routers.home import home
from routers.admin import admin

app = Flask(__name__)
app.config.from_object(config)

db.init_app(app)
# db.create_all(app=app)
migrate = Migrate(app, db)

app.register_blueprint(home)
app.register_blueprint(admin)


@app.before_request
def before_request():
    user_id = session.get('user_id')
    if user_id:
        user = User.query.get(user_id)
        g.user = user


@app.context_processor
def context_processor():
    if hasattr(g, 'user'):
        return {'user': g.user}
    else:
        return {}


def count(s):
    return len(s)


def remove_tag(s):
    return re.sub(r'<[^>]+>', '', s)


app.jinja_env.filters['count'] = count
app.jinja_env.filters['remove_tag'] = remove_tag

if __name__ == '__main__':
    app.run(host='0.0.0.0')
